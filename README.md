# ics-ans-role-conserver-client

Ansible role to install [conserver client](https://www.conserver.com).

## Role Variables

```yaml
conserver_client_host: "localhost"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conserver-client
```

## License

BSD 2-clause
