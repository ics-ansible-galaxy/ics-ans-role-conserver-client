import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


def test_conserver_client(host):
    pkg = "conserver"
    hostnames = ["ics-ans-role-conserver-client-default", "ics-ans-role-conserver-client-ubuntu"]
    if host.ansible.get_variables()["inventory_hostname"] in hostnames:
        pkg = "conserver-client"

    assert host.package(pkg).is_installed

    assert 0 == host.run("console -h").rc
