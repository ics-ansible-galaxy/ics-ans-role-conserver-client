# Nicklas Holmberg, European Spallation Source, 2019
#
# Note that this script is complementary to the conserver autocomplete option
# (see https://www.conserver.com/docs/conserver.cf.man.html): that option allows
# you to type, for example
#
#     console partial_con<return>
#
# in order to load the console `partial_console`. It does not, however, provide
# tab autocompletion, which is what this script provides.

_console_ioc_completions() {
  # Stop autocomplete if one argument already is supplied
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  # Search through the output from 'console -u' and
  # filter by using the awk and only print the first
  # word found
  COMPREPLY=($(compgen -W "$(console -u | awk '{print $1;}')" -- "${COMP_WORDS[1]}"))
}

# autocomplete available for 'console' command
complete -F _console_ioc_completions console
